import gps_reader

def signal_handler(signal, frame):
    print("Stopping app")
    exit(0)

signal.signal(signal.SIGINT, signal_handler) # handles Ctrl+C interupt

gpsd = None # global variable  :-/

if  __name__ == "__main__":
   
   print("Start application to read gps")
   gpsp=GpsPoller()
    
   try:
       gpsp.start()
        
       while True:
           with open("locations.csv","w") as f:
               f.write(str(gpsd.fix.longitude) + "," + str(gpsd.fix.latitude) + "\n")
               print("Wrote: " + str(gpsd.fix.longitude) + "," + str(gpsd.fix.latitude) + "\n")
            
           time.sleep(3)
       
   except(KeyboardInterrupt,SystemExit):
       gpsp.running = False
       gpsp.join()
       print("Stopping")
       exit(0)