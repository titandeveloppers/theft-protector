import Position

class DataCollector:

	dataCollection = None # array of position
	approx = None

	dangerousDistance = 150 # nastavit

	def __init__(self):
		self.dataCollection = []

	def push(self, position):
		"""add to collection
			if it is FIRST sets approx point
			trows Exception on big distance of points			
		"""
		self.dataCollection.append(position)

		if len(self.dataCollection) == 0:
			self.approx = position
		else:
			self.approx.approx(position)
			if self.approx.distance(position) > dangerousDistance:
				raise Exception("Safe distance breached!")



	def clear(self):
		"""clears collection and approx"""
		self.dataCollection = []
		self.approx = None

	def approx(self):
		return self.approx

	def get_lats(self):
		"""return last position in collection"""
		return self.dataCollection[len(DataCollector)-1]