import datetime
import json
import pyproj
from functools import partial
from shapely.geometry import Point
from shapely.ops import transform

class Position:
	""" Position gps """
	x = 0
	y = 0
	timestamp = ""
	project = partial(
		pyproj.transform,
		pyproj.Proj(init='epsg:4326'), # source coordinate system
		pyproj.Proj(init='epsg:5514')) # destination coordinate system

	def __init__(self, gpsdInstance):
		self.x = gpsdInstance.fix.longitude
		self.y = gpsdInstance.fix.latitude
		self.timestamp = datetime.datetime.now()

	def distance(self, position):
		"""Returns absolute value (distance between positions)"""
		p1 = transform(project, Point(self.x, self.y))
		p2 = transform(project, Point(position.x, position.y))
		return p1.distance(p2)

	def approx(self, position):
		"""Approx with another position"""
		self.x += position.x
		self.x /= 2

		self.y += position.y
		self.y /= 2

	def toJson(self):
		"""returns json data for position"""
		data = {
			"timestamp": self.timestamp,
			"coordinates": [self.x, self.y]
		}
		return json.dumps(data)