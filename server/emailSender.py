
__author__ = "Petr Kohout <kohout.peter@seznam.cz>"

import requests, smtplib, ssl

class Email:
	"""
	Sends mail to a client

	use send() method to send mail - it works now

	if you use gmail, you have to allow unsecure app access
	"""
	smtp_server = "smtp.gmail.com"
	port = 587  # For starttls
	sender_email = "petr.mailtesting@gmail.com"
	password = "testingmail"

	subject = ""
	message = ""

	def __init__(self, subject, message):
		self.subject = subject
		self.message = message

	def set_smtp_server(self, server):
		self.smtp_server = server

	def set_sender(self, sender):
		self.sender_email = sender

	def set_password(self, pwd):
		self.password = pwd
	
	def send_simple_message(self, to):
		""" Sends email to specific receiver """
		return requests.post("https://api.mailgun.net/v3/samples.mailgun.org/messages",
			auth=("api", "key-3ax6xnjp29jd6fds4gc373sgvjxteol0"),
			data={"from": "Theft Protector <excited@samples.mailgun.org>",
				"to": [to],
				"subject": self.subject,
				"text": self.message}
			)

	def send_imap(self, to):
		""" Sends email to specific receiver """
		context = ssl.create_default_context()
		with smtplib.SMTP_SSL(self.smtp_server, self.port, context=context) as server:
			server.login(self.sender_email, self.password)
			server.sendmail(self.sender_email, to, self.message)

	def send(self, to):
		""" Sends email to specific receiver  works"""
		header  = 'From: %s \n' % self.sender_email
		header += 'To: %s \n' % to
		header += 'Subject: %s \n' % self.subject
		message = header + self.message

		server = smtplib.SMTP(self.smtp_server, self.port)
		server.starttls();
		server.login(self.sender_email, self.password)
		server.sendmail(self.sender_email, to, message)